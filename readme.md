# DEPRECATED

___This project is no longer maintained as it will be replaced by a new initiative.___


# download-my-jars

Can be used to download all jars within a Maven project. Edit the pom and in the `dependencies` block add all top level dependencies you want to download. Then execute the following the directory with the pom.xml, where OUTPUT_DIR should be replaced with the absolute path to a download directory.

```
mvn dependency:copy-dependencies -DoutputDirectory=OUTPUT_DIR
```
